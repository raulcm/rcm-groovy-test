package cz.bsc.service

import cz.bsc.dao.UserDao
import cz.bsc.domain.User
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

/**
 * Test for {@link BarServiceImpl}.
 */
class BarServiceImplTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none()

    BarServiceImpl createBarService(List<User> users) {
        def userDaoMock = [getAllUsers: { -> users }] as UserDao
        return new BarServiceImpl(userDao: userDaoMock)
    }

    User createUser(String userName, int userAge) {
        return [name: userName, age: userAge] as User
    }

    @Test(expected = IllegalStateException.class)
    void "do not serve beer to user younger than 18"() {
        BarServiceImpl barService = createBarService([createUser('Franta', 17)])
        barService.serveBeerForAll()
    }

    @Test()
    void "All users have been served"() {
        BarServiceImpl barService = createBarService([
                createUser('Franta', 20),
                createUser('Ismael', 30),
                createUser('John', 40)
        ])

        assert barService.serveBeerForAll() == 3
    }

    @Test()
    void "Tells user name on exception"() throws IllegalStateException {
        expectedException.expect(IllegalStateException.class)
        expectedException.expectMessage("User Ismael is too young.")

        BarServiceImpl barService = createBarService([
                createUser('Franta', 20),
                createUser('Ismael', 5),
                createUser('John', 40)
        ])

        barService.serveBeerForAll()
    }

    @Test()
    void "It will not fail with an empty list"() {
        BarServiceImpl barService = createBarService([])
        assert barService.serveBeerForAll() == 0
    }

    @Test(expected = NullPointerException.class)
    void "Fails when UserDao returns null"() {
        BarServiceImpl barService = createBarService(null)
        barService.serveBeerForAll()
    }


    @Test(expected = IllegalStateException.class)
    void "Fails when user has no age"() {
        BarServiceImpl barService = createBarService([new User(name: 'NoAge')])
        barService.serveBeerForAll()
    }
}
